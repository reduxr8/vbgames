Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Collections
Imports System.IO

Public Class Form1
	Inherits Form
	Public WithEvents PlayerBox As PictureBox
	Public WithEvents KeyBox As PictureBox
	Public WithEvents GoalBox As PictureBox
	Public WithEvents Timer1 As System.Windows.Forms.Timer
	Public Pipes As ArrayList = New ArrayList()

	Public Velocity As Integer = 10
	Public HasKey As Boolean = false

	Public Sub New()

		Me.ClientSize = New Size(1280, 720)
		Me.SuspendLayout()

		Timer1 = New System.Windows.Forms.Timer()

		Dim Pipe As PictureBox
		Dim i As Integer
		For i = 0 To 6
			Pipes.Add(New PictureBox())
			Pipes(i).BackColor = Color.Black
		Next

		Pipes(0).Size = New Size(320, 80)
		Pipes(0).Location = New Point(0, 160)

		Pipes(1).Size = New Size(80, 560)
		Pipes(1).Location = New Point(480, 0)

		Pipes(2).Size = New Size(80, 320)
		Pipes(2).Location = New Point(240, 240)

		Pipes(3).Size = New Size(560, 80)
		Pipes(3).Location = New Point(560, 400)

		Pipes(4).Size = New Size(80, 160)
		Pipes(4).Location = New Point(960, 240)

		Pipes(5).Size = New Size(400, 80)
		Pipes(5).Location = New Point(640, 160)

		Pipes(6).Size = New Size(1280, 80)
		Pipes(6).Location = New Point(0, 640)

		For Each Pipe in Pipes
			Me.Controls.Add(Pipe)
		Next

		PlayerBox = New PictureBox()
		PlayerBox.Size = New Size(80, 80)
		PlayerBox.Location = New Point(30, 30)
		PlayerBox.BackColor = Color.Red
		Me.Controls.Add(PlayerBox)

		KeyBox = New PictureBox()
		KeyBox.Size = New Size(80, 80)
		KeyBox.Location = New Point(80, 320)
		KeyBox.BackColor = Color.Yellow
		Me.Controls.Add(KeyBox)

		GoalBox = New PictureBox()
		GoalBox.Size = New Size(80, 80)
		GoalBox.Location = New Point(800, 240)
		GoalBox.BackColor = Color.Blue
		Me.Controls.Add(GoalBox)

		Me.ResumeLayout()

	End Sub

	Private Sub Form1_KeyDown(sender As Object, _
		e As KeyEventArgs) Handles Me.KeyDown

		Dim LastPoint As Point = PlayerBox.Location

		If e.KeyCode = Keys.Right Then
			PlayerBox.Location = New Point(PlayerBox.Location.X + Velocity, _
					PlayerBox.Location.Y)
		End If

		If e.KeyCode = Keys.Left Then
			PlayerBox.Location = New Point(PlayerBox.Location.X - Velocity, _
					PlayerBox.Location.Y)
		End If

		If e.KeyCode = Keys.Down Then
			PlayerBox.Location = New Point(PlayerBox.Location.X, _
					PlayerBox.Location.Y + Velocity)
		End If

		If e.KeyCode = Keys.Up Then
			PlayerBox.Location = New Point(PlayerBox.Location.X, _
					PlayerBox.Location.Y - Velocity)
		End If

		' Prevent Player From Getting Out of Bounds

		If Not PlayerBox.Location.X > 0 And PlayerBox.Location.X < Me.ClientSize.Width Then
			PlayerBox.Location = LastPoint
		End If

		If Not PlayerBox.Location.Y > 0 And PlayerBox.Location.Y < Me.ClientSize.Height Then
			PlayerBox.Location = LastPoint
		End If

		' Pipe Collission
		Dim Pipe As PictureBox
		For Each Pipe in Pipes
			If PlayerBox.Bounds.IntersectsWith(Pipe.Bounds) Then
				PlayerBox.Location = LastPoint
			End If
		Next
	End Sub

	Private Sub Form1_Load(sender As Object, _
		e As EventArgs) Handles MyBase.Load
		Timer1.Enabled = true
		Timer1.Interval = 60
		Timer1.Start()
	End Sub

	Private Sub Timer1_Tick(sender As Object, _
		e As EventArgs) Handles Timer1.Tick
		If PlayerBox.Bounds.IntersectsWith(KeyBox.Bounds) Then
			KeyBox.Visible = false
			HasKey = true
		End If

		If PlayerBox.Bounds.IntersectsWith(GoalBox.Bounds) And Haskey Then
			MessageBox.Show("Finished!")
			Timer1.Stop()
		End If
	End Sub

	Shared Sub Main()
		Application.EnableVisualStyles()
		Application.Run(New Form1())
	End Sub
End Class
